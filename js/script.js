/* Master Mind */

const maxRows = 12;
const maxPegs = 4;
const colors = ['blue', 'green','orange','pink','violet','yellow'];
const keyLetters = ['KeyB', 'KeyG', 'KeyO', 'KeyP', 'KeyV', 'KeyY'];
const keyNumbers = ['Digit1', 'Digit2', 'Digit3', 'Digit4', 'Digit5', 'Digit6'];
const maxColors = colors.length;

var master = [];
var codebreaker  = []
var currentRow = 1;
var currentPeg = 1;
var isPlaying = false;

//	On body load, setup the board
document.onload = init();
//	Start button 
document.getElementById('btnStart').addEventListener('click', startGame);
//	Keyboard listener
document.addEventListener('keydown', key=>{
	if (isPlaying){
		var index = keyLetters.indexOf(key.code);
		if (index == -1 ) index = keyNumbers.indexOf(key.code);
		if (index != -1) pickedColor( colors[ index ] );
	}
});

/**
 * Function to initialize everyrthing
 */
function init(){

	resetBoard();
}

/**
 * Setup the board.
 */
function setupBoard(){

	// get parent
	let divCodebreaker = document.getElementById('codebreaker');

	// loop for row of guesses
	for( r = maxRows; r >= 1; r--){
		// create comment
		divCodebreaker.appendChild( document.createComment(' Guess # ' + r) );

		// create parent #guesses
		let divGuesses = document.createElement('div');
		divGuesses.className = "guesses";
		divGuesses.id = "row_" + r;

		// create parent #outcome
		let divOutcome = document.createElement('div');
		divOutcome.className = "outcome";
		divOutcome.id = "result_" + r;

			// create all pegs
			for (let p = 1; p <= maxPegs; p++){
				// guessing
				let pegGuess = document.createElement('div');
				pegGuess.className = "guess";
				divGuesses.appendChild( pegGuess );

				// outcome
				let pegOutcome = document.createElement('div');
				pegOutcome.className = "peg";
				divOutcome.appendChild( pegOutcome );
			}
		
			divGuesses.appendChild( divOutcome );
			divCodebreaker.appendChild( divGuesses );

	}


}

/**
 * Reset the board.
 */
function resetBoard(){
	// open master cover
	document.getElementById('cover').classList.add("open");

	// disable and hide codemaster colors
	disableCodemaster();

	// show start button
	showStartOptions();

	currentRow = 1;
	currentPeg = 1;
	master = [];
	codebreaker = [];
	isPlaying = false;

}

/**
 * Start the game.
 */
function startGame(){
	
	removeCodemasterGuesses();
	

	// pick random colors for master
	for(let m = 1; m <= maxPegs; m++){
		let randomColor = colors[ getRandomNumber() ];
		master.push( randomColor );
	}
	document.getElementById('cover').classList.remove("open");

	// enable and show codemaster colors

	// hide start button
	showStartOptions(false);

	setupBoard();

	setTimeout(() => {
		
		let codemasterColors = document.getElementById('options').getElementsByClassName('guess');

		for(let c = 0; c < codemasterColors.length; c++){
			codemasterColors[c].disabled = false;
			codemasterColors[c].style.display = "block";
		}
		isPlaying = true;
	}, 1000);
}

/**
 * Generate random numbers
 */
function getRandomNumber(){
	return Math.floor( Math.random() * colors.length );
}

/**
 * Set and show the codebreaker the masters colors
 */
function showMaster(){

	for(let m = 1; m <= master.length; m++){
		document.getElementById('master_' + m).className = "guess";
		document.getElementById('master_' + m).classList.add( 'color', master[m-1] );
	}
	setTimeout(() => {
		document.getElementById('cover').classList.add("open");
	}, 1000);
}

/**
 * Remove all tghe guesses from the master board
 */
function removeMasterGuess(){
	for(let m = 1; m <= master; m++)
		document.getElementById('master_' + masterm).attr('class', 'guess');
}

/**
 * Put the codebreaker color in place
 * @param {string} cbColor Color picked by the codebreaker
 */
function pickedColor(cbColor){
	let picked = document.getElementById('row_' + currentRow).getElementsByTagName('div')[currentPeg-1];

	picked.classList.add('color', cbColor);
	codebreaker.push( cbColor );

	if ( currentPeg++ >= maxPegs){
		checkCodebreaker();
	}

}

/**
 * Validation of the codebreaker guess
 */
function checkCodebreaker(){
	
	let results = document.getElementById('result_' + currentRow).getElementsByTagName('div');
	let currentResult = 0;
	//	correct color, correct location
	for (let p = 0; p < maxPegs; p++ ){
		if ( master[p] == codebreaker[p] ){
			results[currentResult++].classList.add('correct');

		}
	}
	if (currentResult == maxPegs){
		winCodebreaker();
		return;
	}
	//	correct color, wrong location
	let masterCopy = master.map((x) => x); // so we dont alter the real master

	for (let m = 0; m < maxPegs; m++ ){
		if ( masterCopy[m] != codebreaker[m] ){
			

			let index = masterCopy.indexOf(codebreaker[m]);
			if (index != -1 ){
				masterCopy[ index ] = "";
				results[currentResult++].classList.add('incorrect');
			}
		}else {
			masterCopy[m] = "";

		}
	}
	
	//	setup for next row
	codebreaker = [];
	currentRow++
	currentPeg = 1;
	if ( currentRow > maxRows )
		winMaster();


}

function winMaster(){
	setNewMessage("You Lose!", "lose");
	showMaster();
	resetBoard();
}

function winCodebreaker(){
	setNewMessage("You Win!", "win");
	showMaster();
	resetBoard();
}

function disableCodemaster(){
	let codemasterColors = document.getElementById('options').getElementsByClassName('guess');

	for(let c = 0; c < codemasterColors.length; c++){
		codemasterColors[c].disabled = true;
		codemasterColors[c].style.display = "none";
	}

}


function showStartOptions(isStart = true){
	// New Game Button
	document.getElementById('btnStart').disabled = isStart ? "" : "disabled";
	document.getElementById('btnStart').style.display = isStart ? "inline-block" : "none";
}

function removeCodemasterGuesses(){
	setNewMessage();

	// let divChild = document.getElementById('codebreaker').getElementsByClassName('guesses');
	// console.log(divChild);
	for( let c = 1; c <= maxRows; c++ ){
		if (document.getElementById("row_" + (c) ) )
			document.getElementById("row_" + (c)).remove();
	}

}

function setNewMessage (newMsg="", newClass=""){
	document.getElementById('message').innerText = newMsg;
	document.getElementById('message').className = newClass;
}
